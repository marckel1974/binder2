# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'databaseConnectionSetting.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_databaseConnection(object):
    def setupUi(self, databaseConnection):
        databaseConnection.setObjectName("databaseConnection")
        databaseConnection.setWindowModality(QtCore.Qt.ApplicationModal)
        databaseConnection.resize(400, 300)
        databaseConnection.setSizeGripEnabled(True)
        databaseConnection.setModal(True)
        self.buttonBox = QtWidgets.QDialogButtonBox(databaseConnection)
        self.buttonBox.setGeometry(QtCore.QRect(30, 240, 341, 32))
        self.buttonBox.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.buttonBox.setAutoFillBackground(False)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Save)
        self.buttonBox.setCenterButtons(True)
        self.buttonBox.setObjectName("buttonBox")

        self.retranslateUi(databaseConnection)
        self.buttonBox.accepted.connect(databaseConnection.accept)
        self.buttonBox.rejected.connect(databaseConnection.reject)
        QtCore.QMetaObject.connectSlotsByName(databaseConnection)

    def retranslateUi(self, databaseConnection):
        _translate = QtCore.QCoreApplication.translate
        databaseConnection.setWindowTitle(_translate("databaseConnection", "Подключение к БД"))

