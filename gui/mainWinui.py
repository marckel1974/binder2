# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainWinui.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setWindowModality(QtCore.Qt.NonModal)
        MainWindow.resize(767, 586)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 767, 20))
        self.menubar.setObjectName("menubar")
        self.menu = QtWidgets.QMenu(self.menubar)
        self.menu.setObjectName("menu")
        self.menu_2 = QtWidgets.QMenu(self.menubar)
        self.menu_2.setObjectName("menu_2")
        self.menu_3 = QtWidgets.QMenu(self.menubar)
        self.menu_3.setObjectName("menu_3")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.help = QtWidgets.QAction(MainWindow)
        self.help.setObjectName("help")
        self.about = QtWidgets.QAction(MainWindow)
        self.about.setObjectName("about")
        self.databaseConnection = QtWidgets.QAction(MainWindow)
        self.databaseConnection.setObjectName("databaseConnection")
        self.menu.addAction(self.help)
        self.menu.addSeparator()
        self.menu.addAction(self.about)
        self.menu_2.addAction(self.databaseConnection)
        self.menubar.addAction(self.menu_3.menuAction())
        self.menubar.addAction(self.menu_2.menuAction())
        self.menubar.addAction(self.menu.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Binder2"))
        self.menu.setTitle(_translate("MainWindow", "Справка"))
        self.menu_2.setTitle(_translate("MainWindow", "Настройки"))
        self.menu_3.setTitle(_translate("MainWindow", "Файл"))
        self.help.setText(_translate("MainWindow", "Справка"))
        self.about.setText(_translate("MainWindow", "О программе"))
        self.databaseConnection.setText(_translate("MainWindow", "Подключение к БД"))

