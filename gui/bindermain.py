# -*- coding: utf-8 -*
# http://qaru.site/questions/11399194/how-to-open-a-child-window-from-the-main-window
# Главный файл запуска приложения - здесь инициализация
# и "для поддержки некоторых сторонних интерфейсов приложения, таких как обновления или глобальные протоколирования"
#  - ну так в мануале понаписано, я хз

import logging
from PyQt5 import QtCore, QtGui, QtWidgets
from mainWinClass import MainWin, StartDialog


log_file = 'bindermain.log'

logging.basicConfig(format=u'%(filename)s[LINE:%(lineno)d]#'
                           u' %(levelname)-8s [%(asctime)s]'u'%(message)s',
                    level=logging.INFO)

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)

    login = StartDialog()
    if not login.exec_():
        sys.exit(-1)

    mainWin = MainWin()
# где-то здесь должен быть сеттер для коннекта.
# http://qaru.site/questions/3096510/how-to-communicate-or-switch-between-two-windows-in-pyqt
    mainWin.show()
    sys.exit(app.exec_())

