# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'AboutForm.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_About(object):
    def setupUi(self, About):
        About.setObjectName("About")
        About.setWindowModality(QtCore.Qt.ApplicationModal)
        About.resize(264, 141)
        self.label_name = QtWidgets.QLabel(About)
        self.label_name.setGeometry(QtCore.QRect(20, 20, 59, 15))
        self.label_name.setObjectName("label_name")
        self.label_about = QtWidgets.QLabel(About)
        self.label_about.setGeometry(QtCore.QRect(20, 40, 240, 21))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_about.sizePolicy().hasHeightForWidth())
        self.label_about.setSizePolicy(sizePolicy)
        self.label_about.setObjectName("label_about")
        self.label_ver = QtWidgets.QLabel(About)
        self.label_ver.setGeometry(QtCore.QRect(200, 20, 59, 15))
        self.label_ver.setObjectName("label_ver")
        self.label_about2 = QtWidgets.QLabel(About)
        self.label_about2.setGeometry(QtCore.QRect(20, 60, 121, 16))
        self.label_about2.setObjectName("label_about2")
        self.pushButton = QtWidgets.QPushButton(About)
        self.pushButton.setGeometry(QtCore.QRect(80, 100, 80, 23))
        self.pushButton.setObjectName("pushButton")

        self.retranslateUi(About)
        self.pushButton.clicked.connect(About.close)
        QtCore.QMetaObject.connectSlotsByName(About)

    def retranslateUi(self, About):
        _translate = QtCore.QCoreApplication.translate
        About.setWindowTitle(_translate("About", "О программе"))
        self.label_name.setText(_translate("About", "Binder"))
        self.label_about.setText(_translate("About", "Разного рода утилиты по работе"))
        self.label_ver.setText(_translate("About", "2.0"))
        self.label_about2.setText(_translate("About", "с Фрегатом"))
        self.pushButton.setText(_translate("About", "Ok"))

