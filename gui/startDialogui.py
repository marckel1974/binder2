# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'startDialogui.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_StartDialog(object):
    def setupUi(self, StartDialog):
        StartDialog.setObjectName("StartDialog")
        StartDialog.setWindowModality(QtCore.Qt.ApplicationModal)
        StartDialog.resize(308, 238)
        StartDialog.setModal(True)
        self.buttonBox = QtWidgets.QDialogButtonBox(StartDialog)
        self.buttonBox.setGeometry(QtCore.QRect(10, 200, 291, 21))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setCenterButtons(True)
        self.buttonBox.setObjectName("buttonBox")
        self.login = QtWidgets.QLineEdit(StartDialog)
        self.login.setGeometry(QtCore.QRect(20, 110, 260, 23))
        self.login.setObjectName("login")
        self.password = QtWidgets.QLineEdit(StartDialog)
        self.password.setGeometry(QtCore.QRect(20, 150, 260, 23))
        self.password.setEchoMode(QtWidgets.QLineEdit.Password)
        self.password.setObjectName("password")
        self.setting = QtWidgets.QComboBox(StartDialog)
        self.setting.setGeometry(QtCore.QRect(20, 30, 260, 23))
        self.setting.setObjectName("setting")
        self.connectingString = QtWidgets.QLabel(StartDialog)
        self.connectingString.setGeometry(QtCore.QRect(20, 70, 261, 20))
        self.connectingString.setText("")
        self.connectingString.setObjectName("connectingString")

        self.retranslateUi(StartDialog)
        self.buttonBox.accepted.connect(StartDialog.accept)
        self.buttonBox.rejected.connect(StartDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(StartDialog)

    def retranslateUi(self, StartDialog):
        _translate = QtCore.QCoreApplication.translate
        StartDialog.setWindowTitle(_translate("StartDialog", "Подключение к БД"))

