# -*- coding: utf-8 -*

# следуя рекомендации http://qaru.site/questions/11399194/how-to-open-a-child-window-from-the-main-window
# Каждая форма создается в дизайнере, конвертируется и не трогается.
# В этом файле  - сигналы/слоты/коннекты
# Я сюда же и переопределения дочерних окон ибо ваще запутаешься

from PyQt5 import QtGui, QtWidgets, QtCore
import configparser
import logging

from sqlalchemy import *

# Импорт форм и окон
from mainWinui import Ui_MainWindow
from databaseConnectionSettingui import Ui_databaseConnection
from aboutFormui import Ui_About
from startDialogui import Ui_StartDialog


# Глобальное


# класс главного окна:
class MainWin(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QMainWindow.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.AboutWin = None
        self.setup_signals()


    # Здесь определяем все сигналы
    def setup_signals(self):
        self.ui.about.triggered.connect(self.openAbout)
        self.ui.help.triggered.connect(self.openStartDialod)
        self.ui.databaseConnection.triggered.connect(self.openDatabaseConnectionSettingDialog)


    def SomethingMessage(self):
        message = 'Тестовая мессага!'
        reply = QtWidgets.QMessageBox.question(self, 'Уведомление', message,
                                                   QtWidgets.QMessageBox.Ok)
        if reply == QtWidgets.QMessageBox.Ok:
            self.close()
        else:
            print('cancel')

    def openAbout(self):
        if not self.AboutWin:
            self.AboutWin = AboutDialog()
        if self.AboutWin.isVisible():
            print('Hiding')
            self.AboutWin.hide()
        else:
            print('Showing')
            self.AboutWin.show()

    def openDatabaseConnectionSettingDialog(self):
        self.dbConnectionDialog = DatabaseConnectionSettingDialog()
        self.dbConnectionDialog.show()

    def openStartDialod(self):
        self.startDialod = StartDialog()
        self.startDialod.show()
        # self.show()



class AboutDialog(QtWidgets.QDialog):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        self.ui = Ui_About()
        self.ui.setupUi(self)

class DatabaseConnectionSettingDialog(QtWidgets.QDialog):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        self.ui = Ui_databaseConnection()
        self.ui.setupUi(self)


class StartDialog(QtWidgets.QDialog):
    """
    Запускается при старте программы - выбираем вариант подключения к БД
    """

    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        self.ui = Ui_StartDialog()

        self.ui.setupUi(self)
        # читаем конфиг
        config_file = 'config.ini'
        self.config = configparser.ConfigParser()
        config_list = self.config.read(config_file)

        if (config_list == []):
            logging.error(' не могу открыть файл %s', config_file)
        else:
            for section in self.config.sections():
                self.ui.setting.addItem(section)

        # Дефолтное значение комбобокса. Имеет смысл переделать чтоб сохранялось последнее использованное.
        self.comboIndex = 0
        self.ConnectionParam = self.ExtractConnectionStringFromConfig(self.comboIndex)
        self.ui.connectingString.setText(self.ConnectionParam[0]+':'+self.ConnectionParam[1])

        # Функция, в которой храняться сигналы.
        self.setup_signals()

    def setup_signals(self):
        self.ui.setting.activated['int'].connect(self.onActivatedText)
        self.ui.buttonBox.accepted.connect(self.StartDialogYes)


    @QtCore.pyqtSlot(int)
    def onActivatedText(self, index):
        self.comboIndex = index

        self.ConnectionParam = self.ExtractConnectionStringFromConfig(self.comboIndex)
        self.ui.connectingString.setText(self.ConnectionParam[0]+':'+self.ConnectionParam[1])


    def ExtractConnectionStringFromConfig(self, index):
        section = self.config.sections()[index]

        host = self.config.get(section, 'host')
        path = self.config.get(section, 'path')

        return host, path

    def StartDialogYes(self):
        """
        Создает подключение к БД, если всё норм - передает коннект в главное окно. Открывает его естественно.
        :return: Главное окно
        """
        login = self.ui.login.text()
        password = self.ui.password.text()
        host = self.ConnectionParam[0]
        path = self.ConnectionParam[1]

        connect_str = 'firebird+fdb://' + login + ':' + password + '@' + host + ':3050/' + path + '?charset=win1251'
        logging.info(connect_str)

        engine = create_engine(connect_str, echo=True)
        # engine.
        metadata = MetaData(bind=engine)




        # TODO передавать в основное окно параметры подключения по нажатию кнопки.
# https://ru.stackoverflow.com/questions/937292/python-combobox-%D0%9F%D1%80%D0%B8%D0%B2%D1%8F%D0%B7%D0%BA%D0%B0-%D0%B7%D0%BD%D0%B0%D1%87%D0%B5%D0%BD%D0%B8%D0%B9-%D0%BA-%D0%B2%D1%8B%D0%BF%D0%B0%D0%B4%D0%B0%D1%8E%D1%89%D0%B5%D0%BC%D1%83-%D1%81%D0%BF%D0%B8%D1%81%D0%BA%D1%83

        # self.ui.setting.activated['int'].connect(self.onActivatedText)

        # http://www.cyberforum.ru/python-graphics/thread2283639.html
