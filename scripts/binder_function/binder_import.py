#!/usr/bin/python3.5

# -*- coding: utf-8 -*-
# версия 2.1


import requests
import zipfile
import xlrd
from dbfread import DBF
import logging

"""
импорт данных

"""

class Provider(object):
    provider_dict = dict()
    def __init__(self, provider_dict):
        self.provider_dict = provider_dict.copy()
        self.id = self.provider_dict["id"]
        self.provider_name = self.provider_dict["provider_name"]




class PriceList(Provider):
    """
    self.pricelist - список прайслиста.
    вот его надо вытащить и потом пихать в нужное место в БД
    """
    def __init__(self, provider_dict):
        super().__init__(provider_dict)

        self.link = self.provider_dict["link"]
        self.compression = self.provider_dict["compression"]
        self.file_type = self.provider_dict["file_type"]
        self.pricefile = str()
        self.code_column = self.provider_dict["code"]
        self.name_column = self.provider_dict["name"]
        self.qnt_column = self.provider_dict["qnt"]

        self.pricelist = list()


    def DownloadFile(self):
        """
        Скачиваем файл прайса и сохраняем его во временный файл
        Распаковывает при необходимости
        :return: путь к временному файлу
        """

        tmp_folder = "/tmp/"
        def UncompressingPrice(path_to_file):
            """
            Распаковка файла прайс-листа
            :return:
            """
            if self.compression == "zip":
                extracting_price = zipfile.ZipFile(path_to_file).namelist()

                # будем надеяться что в архиве один прайс
                zipfile.ZipFile(self.pricefile).extractall(tmp_folder)
                self.pricefile = tmp_folder + extracting_price[0]


        response = requests.get(self.link, verify=True)

        if response.status_code != 200:
            logging.error('прайс %s недоступен', self.provider_name)
            return
        else:
            filename = self.link[self.link.rfind("/") + 1:]
            self.pricefile = tmp_folder + filename

            with open(self.pricefile, "wb") as f:
                f.write(response.content)

            logging.info('прайс %s скачан', self.provider_name)
            UncompressingPrice(self.pricefile)

            self.ExtractPrice()


        return self.pricefile


    def ExtractPrice(self):
        """
        Извлекает прайс
        :return: self.pricelist - список с данными прайслиста
        """


        if self.file_type == "dbf":
            logging.info("Загружаем DBF файл через self.ExtractDBF()")
            self.ExtractDBF()
        elif self.file_type == "xlsx":
            self.InsertXLSX()
            logging.info("Загружаем xlsx файл через self.ExtractXLSX()")



    def ExtractDBF(self):
        """
        функция загруза dbf файлов
        :return: список self.pricelist
        """

        pricelist = DBF(self.pricefile, load=True)
        for record in pricelist:
            code = str(int(record.get(self.code_column)))
            name = str(record.get(self.name_column))
            qnt = int(record.get(self.qnt_column))
            eancode = 0

            pricestring = (code, name, qnt, eancode)
            self.pricelist.append(pricestring)

        logging.info('%s - считали', self.provider_name)


    def InsertXLSX(self):
        """
        загрузка в БД xlsx файла
        :return: таблица в БД
        """

        # читаем xlsx
        wb = xlrd.open_workbook(self.pricefile)
        sheet = wb.sheet_by_index(0)

        start_row = int(self.provider_dict["start_row"])
        code_column = int(self.code_column)
        name_column = int(self.name_column)
        qnt_column = int(self.qnt_column)
        eancode_column = self.provider_dict["eancode"]



        for row in range (start_row, sheet.nrows):
            # вот если в значимой колонке, хотя бы в одной,
            # не хватает данных или с этими данными что-то не то - пропускаем эту строку.
            # значит пропускаем эту строку
            if (sheet.cell(row, code_column).ctype != xlrd.XL_CELL_EMPTY and
                        sheet.cell(row, name_column).ctype != xlrd.XL_CELL_EMPTY and
                        sheet.cell(row, qnt_column).ctype != xlrd.XL_CELL_EMPTY and
                        sheet.cell(row, qnt_column).ctype == xlrd.XL_CELL_NUMBER):

                code = sheet.cell(row, code_column).value
                name = sheet.cell(row, name_column).value
                qnt = int(sheet.cell(row, qnt_column).value)

                if eancode_column == "NONE":
                    eancode = "0"

                elif sheet.cell(row, int(eancode_column)).ctype == xlrd.XL_CELL_NUMBER:
                    eancode = str(int(sheet.cell(row, int(eancode_column)).value))
                else:
                    eancode = "0"

                pricestring = (code, name, qnt, eancode)
                self.pricelist.append(pricestring)


        logging.info('%s - считали', self.provider_name)


