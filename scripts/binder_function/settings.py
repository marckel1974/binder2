#!/usr/bin/python3.5

# -*- coding: utf-8 -*-
# версия 2.1

"""
В этом файле устанавливаются настройки программы
1. параметры логгирования
2. чтение из конфиг-файла параметров, связанных с подключением к БД и сайту.



"""
import logging
import configparser

config_file = '/etc/binder/binder.conf'  # Путь к конфиг-файлу
log_file = '/var/log/binder/binder.log'  # Путь к лог-файлу

# На этапе отладки можно убрать "filename=log_file"
logging.basicConfig(format=u'%(filename)s[LINE:%(lineno)d]#'
                           u' %(levelname)-8s [%(asctime)s]'u'%(message)s',
                    level=logging.INFO, filename=log_file)

# reading config file
config = configparser.ConfigParser()

if (config.read(config_file) == []):
    logging.error(' не могу открыть файл %s', config_file)

try:
    __firebird_config = config['firebird']

except:
    logging.error(' секция firebird отсутствует в файле %s', config_file)

# местоположение файлов выгрузки
try:
    __files_config = config['files']

except:
    logging.error(' секция files отсутствует в файле %s', config_file)

try:
    __site_config = config['site']
except:
    logging.error('секция site отсутствует в файле %s', config_file)

# файлы выгрузки
needs_xlsx = __files_config['needs']
pricelist = __files_config['pricelist']
jsonpricelist = __files_config['jsonpricelist']

sitepricelist = __site_config['sitepricelist']

host = __site_config['host']
port = int(__site_config['site_port'])
site_password = __site_config['site_passw']
site_user = __site_config['site_user']
sitejsonpricelist = __site_config['jsonpricelist']

tmp_folder = __files_config['tmp']


# database connection settings
firebird_login = __firebird_config['login']
firebird_password = __firebird_config['password']
firebird_host = __firebird_config['host']
firebird_path = __firebird_config['path']


