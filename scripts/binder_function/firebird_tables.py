#!/usr/bin/python3.5

# -*- coding: utf-8 -*-
# версия 2.1

"""
классы для работы с файрберд
параметры подключения тащатся из
"""

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import *

from openpyxl import Workbook
from openpyxl.utils import get_column_letter

import datetime
import paramiko
import logging
import json

Base = declarative_base()

Session = sessionmaker()

# Делаем словарь для выковыривания левых символов из прайса - то что в базу файрберда не лезет
chars_to_remove = str.maketrans('ø×Ø℃∅', 'DxDCD', '²"˚̊°ºΩ')


class GoodAltnamesTable(Base):
    """ Отображение таблицы GOOD$ALTNAMES"""

    __tablename__ = 'GOOD$ALTNAMES'
    id = Column(Integer, primary_key=True)
    good_id = Column(Integer)
    face_id = Column(Integer)
    code = Column(String(40))
    name = Column(String(300))

    def DeleteDoubleItem(good_id):
        """
        Удаление дубликатов
        """
        session = Session()

        for i in session.query(func.min(GoodAltnamesTable.id)).filter(
                GoodAltnamesTable.good_id == good_id):
            session.query(GoodAltnamesTable).filter(GoodAltnamesTable.id == i[0]).delete()
            session.commit()
        session.close()

    def Good_AltnamesUpdate(stock_id, code, name):
        """
        Обновляет альтернативные имена
        """
        session = Session()

        name = str(name).translate(chars_to_remove)[0:100]

        session.query(GoodAltnamesTable) \
            .filter(GoodAltnamesTable.face_id == stock_id) \
            .filter(GoodAltnamesTable.code == code).update({GoodAltnamesTable.name: name})
        session.commit()
        session.close()


class BarcodeTable(Base):
    """
    Отображение таблицы Code
    """
    __tablename__ = 'CODES'
    id = Column(Integer, primary_key=True)
    obj_id = Column(Integer)
    code = Column(String(40))
    type_id = Column(Integer)
    defaultflag = Column(Integer)

    def SaveBarcode(good_id, barcode):
        """
        Записывает barcode с проверкой
        :param barcode:
        :return:
        """

        def CheckBarcode(good_Id, barcode):
            """
            проверяем есть ли такой Ббаркод
            :param barcode
            :return: True если есть, False если нету
            """
            session = Session()

            for res in session.query(BarcodeTable) \
                    .filter(BarcodeTable.obj_id == good_Id) \
                    .filter(BarcodeTable.code == barcode):
                session.close()
                return True

            session.close()
            return False

        session = Session()

        if barcode != 0:
            if not CheckBarcode(good_id, barcode):
                session.add(BarcodeTable(obj_id=good_id, type_id=1, code=barcode, defaultflag=0))

        session.commit()
        session.close()


class GoodsTable(Base):
    """
    отображение таблицы Goods
    """
    __tablename__ = 'GOODS'
    id = Column(Integer, primary_key=True)
    code = Column(String(40))
    name = Column(String(162))
    qnt = Column(Float)
    group_id = Column(Integer, ForeignKey('goodgroupstable.id'))


class GoodGroupsTable(Base):
    """
    отображение таблицы goodgroups
    """
    __tablename__ = 'GOODSGROUPS'
    id = Column(Integer, primary_key=True)
    name = Column(String(100))


class QuantsTable(Base):
    """
    Отображение таблицы Quants
    """

    __tablename__ = 'QUANTS'
    good_id = Column(Integer, primary_key=True)
    stock_id = Column(Integer, primary_key=True)
    series_id = Column(Integer, primary_key=True)
    qnt = Column(Float)

    def CheckDoubleItem(good_id, stock_id):
        """
        проверяет - есть ли в QUANTS запись с таким good_id и stock_id
        если есть - значит запись задвоена, видимо в Альтнеймз что-то не так

        возвращает  True если повтор есть, False - если нет.
        """

        session = Session()

        for dupl in session.query(QuantsTable.good_id).filter(QuantsTable.stock_id == stock_id).filter(
                QuantsTable.good_id == good_id):
            session.close()
            return True

        session.close()
        return False

    def DeleteStockGoods(stock_id):
        """
        Удаляет товары с данного стока
        DELETE FROM QUANTS WHERE QUANTS.STOCK_ID <> 37 AND QUANTS.STOCK_ID = stock_id

        """
        session = Session()
        session.query(QuantsTable).filter(QuantsTable.stock_id != "37") \
            .filter(QuantsTable.stock_id == stock_id).delete()
        session.commit()
        session.close()

    def DeleteOldGoods():
        """
        Удаляет старые товары
        DELETE FROM QUANTS WHERE QUANTS.STOCK_ID <> 37
        """

        session = Session()
        session.query(QuantsTable).filter(QuantsTable.stock_id != "37").delete()
        session.commit()
        session.close()

    def UpdateQuants(code, name, stock_id, qnt, barcode):
        """
        Добавляет новые значения в Quants со всеми проверками и прочим.

        """

        session = Session()
        qnt = int(qnt)

        if qnt > 0:
            for good in session.query(GoodAltnamesTable).filter(GoodAltnamesTable.code == code).filter(
                    GoodAltnamesTable.face_id == stock_id):

                # проверяем, а вдруг есть такой товар уже
                # print('good_id=', good.good_id, 'barcode=', barcode, 'name', name)
                if QuantsTable.CheckDoubleItem(good.good_id, stock_id):
                    # Удаляем дубликат
                    GoodAltnamesTable.DeleteDoubleItem(good.good_id)
                    session.commit()

                else:
                    # Добавляем в Quants
                    session.add(QuantsTable(good_id=good.good_id, stock_id=stock_id, series_id=0, qnt=qnt))
                    GoodAltnamesTable.Good_AltnamesUpdate(stock_id, code, name)

                    # Добавляем баркод
                    BarcodeTable.SaveBarcode(good.good_id, barcode)

                    session.commit()

        session.close()


class StockMinMaxTable(Base):
    """отобрежение таблицы stockminmax
    """
    __tablename__ = 'STOCKSMINMAX'
    stock_id = Column(Integer, primary_key=True)
    good_id = Column(Integer, primary_key=True)
    minqnt = Column(Integer)
    maxqnt = Column(Integer)


class FacesTable(Base):
    """Отображение таблицы Faces"""
    __tablename__ = 'FACES'
    id = Column(Integer, primary_key=True)
    name = Column(String(80))


class PricesTable(Base):
    """
    Таблица Price
    """
    __tablename__ = 'PRICES'
    pricelist_id = Column(Integer, primary_key=True)
    good_id = Column(Integer, primary_key=True)
    dt = Column(Date, primary_key=True)
    price_i = Column(DECIMAL(10, 2))


def XlsxAlignment(worksheet, addlength):
    """
    подпрограмка для выравнивания колонок в таблице, чтоб все буквы было видно
    worksheet  - таблица, которую надо изуродвать
    addlength - сколько добавить к колонке

    as_text - локальная функция для выяснения длины колонки

    :return: ту же таблицу, но выравненную
    """

    def as_text(value):
        if value is None:
            return ""
        return str(value)

    for column_cells in worksheet.columns:
        column = column_cells[0].column  # Get the column index

        length = max(len(as_text(cell.value)) for cell in column_cells)
        worksheet.column_dimensions[get_column_letter(column)].width = length + addlength



def ExportNeeds(needs_xlsx):
    session = Session()

    #  создаем файл под потребности
    wb = Workbook()
    ws = wb.active

    ws.append(
        ["группа", "код", "название", "в наличии", "мин", "макс", "поставщик", "имя у поставщика", "код постащика"])

    res = session.query(GoodGroupsTable.name, GoodsTable.code, GoodsTable.name, GoodsTable.qnt,
                        StockMinMaxTable.minqnt, StockMinMaxTable.maxqnt, FacesTable.name, GoodAltnamesTable.name,
                        GoodAltnamesTable.code) \
        .join(GoodsTable, GoodsTable.group_id == GoodGroupsTable.id) \
        .join(StockMinMaxTable, StockMinMaxTable.good_id == GoodsTable.id) \
        .join(GoodAltnamesTable, GoodAltnamesTable.good_id == GoodsTable.id) \
        .join(FacesTable, FacesTable.id == GoodAltnamesTable.face_id) \
        .filter(StockMinMaxTable.minqnt.isnot(None)).filter(StockMinMaxTable.minqnt > 0) \
        .filter(StockMinMaxTable.maxqnt.isnot(None)).filter(StockMinMaxTable.maxqnt > 0) \
        .filter(GoodsTable.qnt <= StockMinMaxTable.minqnt)

    for row in res:
        ws.append(list(row))

    # выравниваем таблицу
    XlsxAlignment(ws, 2)
    # Записываем xlsx

    wb.save(needs_xlsx)
    session.close_all()
    logging.info('файл потребностей сохранен: %s', needs_xlsx)


def ExportPrice(host, port, site_user, site_password, jsonpricelist, pricelist, sitepricelist, sitejsonpricelist):
    """
        Выгрузка прайса в эксель на сайт
        Выгрузку прайса в json на einfo
        :return:
        """
    """
        прайсы во Фрегате храняться в таблице prices
        """

    # Файл прайс-листа
    wb = Workbook()
    ws = wb.active

    session = Session()
    transport = paramiko.Transport(host, port)
    transport.connect(username=site_user, password=site_password)
    sftp = paramiko.SFTPClient.from_transport(transport)

    # Заголовок прайса
    ws.append(["группа", "название", "код", "цена розн", "цена опт", "количество", "срок поставки"])

    # открываем (создаем)json
    js = list()
    jsonstr = dict()

    fjs = open(jsonpricelist, "w")

    for goodname, goodcode, good_id, goodsgroup, qnt, stock_id \
            in session.query(GoodsTable.name, GoodsTable.code, GoodsTable.id,
                             GoodGroupsTable.name, QuantsTable.qnt, QuantsTable.stock_id) \
            .join(GoodGroupsTable, GoodGroupsTable.id == GoodsTable.group_id) \
            .join(QuantsTable, QuantsTable.good_id == GoodsTable.id) \
            .filter(QuantsTable.qnt > 0) \
            .filter(GoodGroupsTable.id != 80004) \
            .filter(GoodGroupsTable.id != 29770) \
            .filter(GoodGroupsTable.id != 1932) \
            .filter(GoodGroupsTable.id != 2132) \
            .filter(GoodGroupsTable.id != 2037) \
            .filter(GoodGroupsTable.id != 2129) \
            .filter(GoodGroupsTable.id != 2042) \
            .filter(GoodGroupsTable.id != 2103) \
            .order_by(GoodGroupsTable.name):

        retailprice = 0
        wholesaleprice = 0
        dtmax = datetime.date.fromordinal(1)
        deliverytime = '3-7 раб.дней'
        delivery_json = 1
        # 37 - наш склад
        if stock_id == 37:
            deliverytime = 'В наличии'
            delivery_json = 0

        for res in session.query(PricesTable.good_id, PricesTable.dt, PricesTable.pricelist_id,
                                 PricesTable.price_i).filter(PricesTable.good_id == good_id):
            if dtmax < res.dt:
                dtmax = res.dt
                if res.pricelist_id == 1:
                    retailprice = res.price_i
                elif res.pricelist_id == 2:
                    wholesaleprice = res.price_i

                if retailprice < wholesaleprice:
                    retailprice = wholesaleprice

        qnt = int(qnt)
        record_string = (goodsgroup, goodname, goodcode, retailprice, wholesaleprice, qnt, deliverytime)

        ws.append(record_string)

        jsonstr = dict([("title", goodname),
                        ("category", goodsgroup),
                        ("price_opt", wholesaleprice),
                        ("price_roz", retailprice),
                        ("stock", int(qnt)),
                        ("delivery", delivery_json)])

        print(json.dumps(jsonstr, ensure_ascii=False), file=fjs)

    #  ширина колонок
    XlsxAlignment(ws, 1)

    wb.save(pricelist)
    logging.info(' %s - сделано', pricelist)

    sftp.put(pricelist, sitepricelist)
    sftp.put(jsonpricelist, sitejsonpricelist)
    sftp.close()

    logging.info('прайс-листы успешно загружены по адресу %s, %s', sitepricelist, sitejsonpricelist)

