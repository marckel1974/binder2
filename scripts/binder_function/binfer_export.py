#!/usr/bin/python3.5

# -*- coding: utf-8 -*-
# версия 2.1

# TODO: экпорт данных
# TODO: переделать конфиги - вытащить их в settings

"""
все экспорты из БД.
"""

import logging
# Было так:


class DataExport(object):
    """
    класс для выгрузки потребностей
    """
    def __init__(self):

        if (config.read(config_file) == []):
            logging.error(' не могу открыть файл %s', config_file)

        try:
            files_config = config['files']

        except:
            logging.error('секция files отсутствует в файле %s', config_file)

        try:
            site_config = config['site']

        except:
            logging.error('секция site отсутствует в файле %s', config_file)

        self.needs_xlsx = files_config['needs']
        self.pricelist = files_config['pricelist']
        self.jsonpricelist = files_config['jsonpricelist']


        self.sitepricelist = site_config['sitepricelist']


        self.host = site_config['host']
        self.port = int(site_config['site_port'])
        self.site_passwd = site_config['site_passw']
        self.site_user = site_config['site_user']
        self.sitejsonpricelist = site_config['jsonpricelist']


    def XlsxAlignment(worksheet , addlength):
        """
        подпрограмка для выравнивания колонок в таблице, чтоб все буквы было видно
        worksheet  - таблица, которую надо изуродвать
        addlength - сколько добавить к колонке

        as_text - локальная функция для выяснения длины колонки

        :return: ту же таблицу, но выравненную
        """

        def as_text(value):
            if value is None:
                return ""
            return str(value)

        for column_cells in worksheet.columns:
            length = max(len(as_text(cell.value)) for cell in column_cells)
            worksheet.column_dimensions[column_cells[0].column].width = length+addlength


    def ExportNeeds(self):

        session = Session()

        #  создаем файл под потребности
        wb = Workbook()
        ws = wb.active

        ws.append(["группа", "код", "название", "в наличии", "мин", "макс", "поставщик", "имя у поставщика", "код постащика"])

        res = session.query(GoodGroupsTable.name, GoodsTable.code, GoodsTable.name, GoodsTable.qnt,
                            StockMinMaxTable.minqnt, StockMinMaxTable.maxqnt, FacesTable.name, Good_AltnamesTable.name, Good_AltnamesTable.code)\
            .join(GoodsTable, GoodsTable.group_id == GoodGroupsTable.id)\
            .join(StockMinMaxTable, StockMinMaxTable.good_id == GoodsTable.id)\
            .join(Good_AltnamesTable, Good_AltnamesTable.good_id == GoodsTable.id)\
            .join(FacesTable, FacesTable.id == Good_AltnamesTable.face_id)\
            .filter(StockMinMaxTable.minqnt.isnot(None)).filter(StockMinMaxTable.minqnt > 0)\
            .filter(StockMinMaxTable.maxqnt.isnot(None)).filter(StockMinMaxTable.maxqnt > 0)\
            .filter(GoodsTable.qnt <= StockMinMaxTable.minqnt)

        for row in res:
            ws.append(list(row))

        # выравниваем таблицу
        DataExport.XlsxAlignment(ws, 1)
        # Записываем xlsx

        wb.save(self.needs_xlsx)
        session.close_all()
        logging.info('файл потребностей сохранен: %s', self.needs_xlsx)


    def ExportPrice(self):
        """
        Выгрузка прайса в эксель на сайт
        Выгрузку прайса в json на einfo
        :return:
        """
        """
        прайсы во Фрегате храняться в таблице prices
        """

        # Файл прайс-листа
        wb = Workbook()
        ws = wb.active

        session = Session()
        transport = paramiko.Transport(self.host, self.port)
        transport.connect(username=self.site_user, password=self.site_passwd)
        sftp = paramiko.SFTPClient.from_transport(transport)

        # Заголовок прайса
        ws.append(["группа","название", "код", "цена розн", "цена опт", "количество", "срок поставки"])


        # открываем (создаем)json
        js = list()
        jsonstr = dict()

        fjs = open(self.jsonpricelist, "w")

        for goodname, goodcode, good_id, goodsgroup, qnt, stock_id \
                in session.query(GoodsTable.name, GoodsTable.code, GoodsTable.id,
                                 GoodGroupsTable.name, QuantsTable.qnt, QuantsTable.stock_id)\
                .join(GoodGroupsTable, GoodGroupsTable.id == GoodsTable.group_id)\
                .join(QuantsTable, QuantsTable.good_id == GoodsTable.id)\
                .filter(QuantsTable.qnt > 0) \
                .filter(GoodGroupsTable.id != 80004) \
                .filter(GoodGroupsTable.id != 29770) \
                .filter(GoodGroupsTable.id != 1932) \
                .filter(GoodGroupsTable.id != 2132) \
                .filter(GoodGroupsTable.id != 2037) \
                .filter(GoodGroupsTable.id != 2129) \
                .filter(GoodGroupsTable.id != 2042) \
                .filter(GoodGroupsTable.id != 2103) \
                .order_by(GoodGroupsTable.name):

            retailprice = 0
            wholesaleprice = 0
            dtmax = datetime.date.fromordinal(1)
            deliverytime = '3-7 раб.дней'
            delivery_json = 1
            # 37 - наш склад
            if stock_id == 37:
                deliverytime = 'В наличии'
                delivery_json = 0


            for res in session.query(PricesTable.good_id, PricesTable.dt, PricesTable.pricelist_id,
                                     PricesTable.price_i).filter(PricesTable.good_id == good_id):
                if dtmax < res.dt:
                    dtmax = res.dt
                    if res.pricelist_id == 1:
                        retailprice = res.price_i
                    elif res.pricelist_id == 2:
                        wholesaleprice = res.price_i

                    if retailprice < wholesaleprice:
                        retailprice = wholesaleprice

            qnt = int(qnt)
            record_string = (goodsgroup, goodname, goodcode, retailprice, wholesaleprice, qnt, deliverytime)

            ws.append(record_string)


            jsonstr = dict([("title", goodname),
                            ("category", goodsgroup),
                            ("price_opt", wholesaleprice),
                            ("price_roz", retailprice),
                            ("stock", int(qnt)),
                            ("delivery", delivery_json)])

            print(json.dumps(jsonstr, ensure_ascii=False), file=fjs)

        #  ширина колонок
        DataExport.XlsxAlignment(ws, 1)


        wb.save(self.pricelist)
        logging.info(' %s - сделано', self.pricelist)

        sftp.put(self.pricelist, self.sitepricelist)
        sftp.put(self.jsonpricelist, self.sitejsonpricelist)
        sftp.close()

        logging.info('прайс-листы успешно загружены по адресу %s, %s', self.sitepricelist, self.sitejsonpricelist)

