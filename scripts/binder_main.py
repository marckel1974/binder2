# -*- coding: utf-8 -*-
# версия 2.1

# TODO странно работает выгрузка json

"""
файлы настройки: /etc/binder/binder.conf - то что зависит от компа
settings - настройки прайс-листов и ь.д.
"""


from binder_function.settings import firebird_login, firebird_password, firebird_host, firebird_path
from binder_function.settings import needs_xlsx
from binder_function.settings import host, port, site_user, site_password, jsonpricelist, pricelist, sitepricelist, sitejsonpricelist
from binder_function.firebird_tables import create_engine, MetaData, Session, QuantsTable, GoodGroupsTable, GoodsTable
from binder_function.firebird_tables import StockMinMaxTable, PricesTable, FacesTable, GoodAltnamesTable
from binder_function.firebird_tables import ExportPrice, ExportNeeds
from binder_function.binder_import import PriceList
import json

from os import path

parent_dir = path.dirname(path.abspath(__file__))


if __name__ == "__main__":
    connect_str = 'firebird+fdb://' + firebird_login + ':' + firebird_password + '@' + \
                  firebird_host + ':3050/' + firebird_path + '?charset=win1251'

    engine = create_engine(connect_str, echo=False)
    metadata = MetaData(bind=engine)
    Session.configure(bind=engine)


    with open(parent_dir + '/binder_function/providers.json') as f:
        providers = json.load(f)

    for i in providers:
        pr = PriceList(i)

        QuantsTable.DeleteStockGoods(pr.id)

        pr.DownloadFile()

        for i in pr.pricelist:
            code, name, qnt, eancode = i
            # Заталкиваем в таблицу Quants


            QuantsTable.UpdateQuants(code, name, pr.id, qnt, eancode)


    ExportNeeds(needs_xlsx)

    ExportPrice(host, port, site_user, site_password, jsonpricelist, pricelist, sitepricelist, sitejsonpricelist)

