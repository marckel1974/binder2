# -*- coding: utf-8 -*-

from sqlalchemy import create_engine
import configparser
import logging

config_file = '/etc/binder/binder.conf'  # Путь к конфиг-файлу

# Читаем конфиг и по возможности проверяем
config = configparser.ConfigParser()

log_file = '/var/log/binder/binder.log'

logging.basicConfig(format=u'%(filename)s[LINE:%(lineno)d]#'
                           u' %(levelname)-8s [%(asctime)s]'u'%(message)s',
                    level=logging.INFO, filename=log_file)

if (config.read(config_file) == []):
    logging.error(' не могу открыть файл %s', config_file)

try:
    __firebird_config = config['firebird']

except:
    logging.error(' секция firebird отсутствует в файле %s', config_file)

connect_str = 'firebird+fdb://' + __firebird_config['login'] + ':' + __firebird_config['password'] + '@' + \
                    __firebird_config['host'] + ':3050/' + __firebird_config['path'] + '?charset=win1251'

engine = create_engine(connect_str, echo=False)


# местоположение дополнительных файлов
try:
    __files_config = config['files']

except:
    logging.error(' секция files отсутствует в файле %s', config_file)

tmp_folder = __files_config['tmp']

# Делаем словарь для выковыривания левых символов из прайса - то что в базу файрберда не лезет
chars_to_remove = str.maketrans('ø×Ø℃∅', 'DxDCD', '²"˚̊°ºΩ')


