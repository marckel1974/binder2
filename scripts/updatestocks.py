# -*- coding: utf-8 -*-
# версия 2

# Todo сделать подгруз закупочных цен

from binder_setting import connect_str
from binder_setting import config, config_file, tmp_folder
from binder_setting import logging, chars_to_remove, log_file
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Numeric, DECIMAL, DateTime
from sqlalchemy import Sequence
from sqlalchemy.orm import sessionmaker
from sqlalchemy import *
from openpyxl import Workbook
import json
import datetime


import zipfile
# from pycbrf.toolbox import ExchangeRates
import requests
import re
import xlrd
from dbfread import DBF
import paramiko


Base = declarative_base()
engine = create_engine(connect_str, echo=False)

metadata = MetaData(bind=engine)

Session = sessionmaker()
Session.configure(bind=engine)


class Good_AltnamesTable(Base):
    """ Отображение таблицы GOOD$ALTNAMES"""

    __tablename__ = 'GOOD$ALTNAMES'
    id = Column(Integer, primary_key=True)
    good_id = Column(Integer)
    face_id = Column(Integer)
    code = Column(String(40))
    name = Column(String(300))

    def DeleteDoubleItem(good_id):
        """
        Удаление дубликатов
        """
        session = Session()

        for i in session.query(func.min(Good_AltnamesTable.id)).filter(
                        Good_AltnamesTable.good_id == good_id):

            session.query(Good_AltnamesTable).filter(Good_AltnamesTable.id == i[0]).delete()
            session.commit()
        session.close()


    def Good_AltnamesUpdate(stock_id, code, name):
        """
        Обновляет альтернативные имена
        """
        session = Session()

        name = str(name).translate(chars_to_remove)[0:100]

        session.query(Good_AltnamesTable)\
            .filter(Good_AltnamesTable.face_id == stock_id)\
            .filter(Good_AltnamesTable.code == code).update({Good_AltnamesTable.name: name})
        session.commit()
        session.close()


class BarcodeTable(Base):
    """
    Отображение таблицы Code
    """
    __tablename__ = 'CODES'
    id = Column(Integer, primary_key=True)
    obj_id = Column(Integer)
    code = Column(String(40))
    type_id = Column(Integer)
    defaultflag = Column(Integer)

    def SaveBarcode(good_id, barcode):
        """
        Записывает barcode с проверкой
        :param barcode:
        :return:
        """

        def CheckBarcode(good_Id, barcode):
            """
            проверяем есть ли такой Ббаркод
            :param barcode
            :return: True если есть, False если нету
            """
            session = Session()

            for res in session.query(BarcodeTable) \
                    .filter(BarcodeTable.obj_id == good_Id) \
                    .filter(BarcodeTable.code == barcode):
                session.close()
                return True

            session.close()
            return False


        session = Session()

        if barcode != 0:
            if not CheckBarcode(good_id, barcode):
                session.add(BarcodeTable(obj_id=good_id, type_id=1, code=barcode, defaultflag=0))

        session.commit()
        session.close()


class GoodsTable(Base):
    """
    отображение таблицы Goods
    """
    __tablename__ = 'GOODS'
    id = Column(Integer, primary_key=True)
    code = Column(String(40))
    name = Column(String(162))
    qnt = Column(Float)
    group_id = Column(Integer, ForeignKey('goodgroupstable.id'))

class GoodGroupsTable(Base):
    """
    отображение таблицы goodgroups
    """
    __tablename__ = 'GOODSGROUPS'
    id = Column(Integer, primary_key=True)
    name = Column(String(100))

class QuantsTable(Base):
    """
    Отображение таблицы Quants
    """

    __tablename__ = 'QUANTS'
    good_id = Column(Integer, primary_key=True)
    stock_id = Column(Integer, primary_key=True)
    series_id = Column(Integer, primary_key=True)
    qnt = Column(Float)

    def CheckDoubleItem(good_id, stock_id):
        """
        проверяет - есть ли в QUANTS запись с таким good_id и stock_id
        если есть - значит запись задвоена, видимо в Альтнеймз что-то не так

        возвращает  True если повтор есть, False - если нет.
        """

        session = Session()

        for dupl in session.query(QuantsTable.good_id).filter(QuantsTable.stock_id == stock_id).filter(QuantsTable.good_id == good_id):
            session.close()
            return True

        session.close()
        return False



    def DeleteOldGoods():
        """
        Удаляет старые товары
        DELETE FROM QUANTS WHERE QUANTS.STOCK_ID <> 37
        """

        session = Session()
        session.query(QuantsTable).filter(QuantsTable.stock_id != "37").delete()
        session.commit()
        session.close()

    def UpdateQuants(code, name, stock_id, qnt, barcode):
        """
        Добавляет новые значения в Quants со всеми проверками и прочим.

        """

        session = Session()
        qnt = int(qnt)

        if qnt > 0:
            for good in session.query(Good_AltnamesTable).filter(Good_AltnamesTable.code == code).filter(
                            Good_AltnamesTable.face_id == stock_id):

                # проверяем, а вдруг есть такой товар уже
                # print('good_id=', good.good_id, 'barcode=', barcode, 'name', name)
                if QuantsTable.CheckDoubleItem(good.good_id, stock_id):
                    # Удаляем дубликат
                    Good_AltnamesTable.DeleteDoubleItem(good.good_id)
                    session.commit()

                else:
                    # Добавляем в Quants
                    session.add(QuantsTable(good_id=good.good_id, stock_id=stock_id, series_id=0, qnt=qnt))
                    Good_AltnamesTable.Good_AltnamesUpdate(stock_id, code, name)

                    # Добавляем баркод
                    BarcodeTable.SaveBarcode(good.good_id, barcode)

                    session.commit()
        session.close()

class StockMinMaxTable(Base):
    """отобрежение таблицы stockminmax
    """
    __tablename__ = 'STOCKSMINMAX'
    stock_id = Column(Integer, primary_key=True)
    good_id = Column(Integer, primary_key=True)
    minqnt = Column(Integer)
    maxqnt = Column(Integer)

class FacesTable(Base):
    """Отображение таблицы Faces"""
    __tablename__ = 'FACES'
    id = Column(Integer, primary_key=True)
    name = Column(String(80))

class PricesTable(Base):
    """
    Таблица Price
    """
    __tablename__ = 'PRICES'
    pricelist_id = Column(Integer, primary_key=True)
    good_id = Column(Integer, primary_key=True)
    dt = Column(Date, primary_key=True)
    price_i = Column(DECIMAL(10, 2))

class Provider(object):
    """
    Объект класса - собственно поставщик.
    аттрибуты - код его в файле установок.
    на основании этого кода получаем параметры по которым читать прайс

    """
    id_provider = str()  # группа в файле настроек
    stock_id = int()  # идентификатор в БД
    link = str()  # ссылка на прайс
    compressing_type = str()  # чем сжат прайc


    def __init__(self, id_provider):

        if (config.read(config_file) == []):
            logging.error(' не могу открыть файл %s', config_file)

        try:
            stock_config = config[id_provider]

        except:
            logging.error('секция %s отсутствует в файле', id_provider)

        self.id_provider = id_provider
        self.code = stock_config['code']
        self.name = str(stock_config['name'])
        self.qnt = stock_config['qnt']
        self.price = stock_config['price']
        self.image = str(stock_config['image'])
        self.stock_id = int(stock_config['stock_id'])
        self.goodsgroup = str(stock_config['goodsgroup'])
        # self.group_id = stock_config['group_id']
        self.subgroup = str(stock_config['subgroup'])
        # self.subgroup_id = stock_config['subgroup_id']
        self.recommended_price = stock_config['recommended_price']
        self.packaging = stock_config['packaging']
        self.link = stock_config['link']
        self.type_file = str()
        self.eancode = stock_config['eancode']



    def __repr__(self):
        return "<Provider('%s', '%s', '%s', '%s', '%s', '%s')>" % \
               (self.id_provider, self.stock_id, self.link, self.type_file, self.compressing_type, self.stock_map)

    def UncompressingPrice(self):
        # разархивируем если ннадо
        if self.type_file == "zip":
            #  разархивируем
            extracting_price = zipfile.ZipFile(self.pricefile).namelist()

            # будем надеяться что в архиве один прайс
            zipfile.ZipFile(self.pricefile).extractall(tmp_folder)
            self.pricefile = tmp_folder + extracting_price[0]


    def FileTypeRecognition(self):

        self.type_file = self.pricefile[self.pricefile.rfind(".") + 1:]
        # разархивируем
        if self.type_file == 'zip':
            self.UncompressingPrice()
            self.FileTypeRecognition()
        # logging.info("FileTypeRecognition")

    def SavePrice(self):
        # self.FileTypeRecognition()

        if self.id_provider == "zip-2002":
            self.InsertZip()
        elif self.id_provider == "sds":
            self.InsertSDS()
        elif self.id_provider == "sourcebatteries":
            self.InsertSourseBattaries()
        elif self.id_provider == "compel":
            self.InsertCompel()
        elif self.id_provider == "rct":
            self.InsertRct()

        else:
            print("Не знаю такого поставщика")

    def InsertZip(self):
        """здесь собственно заталкиваем ЗИП в БД

        """
        response = requests.get(self.link)

        if response.status_code != 200:
            return
        else:
            filename = self.link[self.link.rfind("/") + 1:]
            self.pricefile = tmp_folder + filename

            with open(self.pricefile, "wb") as f:
                f.write(response.content)

            logging.info('прайс %s скачан', self.id_provider)

            self.FileTypeRecognition()


            pricelist = DBF(self.pricefile, load=True)
            for record in pricelist:
                code = str(int(record.get(self.code)))
                name = str(record.get(self.name))
                image = "http://zip-2002.ru/bmp/"+str(int(record.get(self.image)))+".jpg"
                qnt = int(record.get(self.qnt))
                price = float(record.get(self.price))
                goodsgroup = str(record.get(self.goodsgroup))
                subgroup = str(record.get(self.subgroup))
                recommended_price = float(record.get(self.recommended_price))
                eancode = 0

                QuantsTable.UpdateQuants(code, name, self.stock_id, qnt, eancode)

            logging.info('%s - затолкали', self.id_provider)


    def InsertSDS(self):
        """
        здесь заталкиваем SDS в БД
        :return:
        """

        response = requests.get(self.link)

        if response.status_code != 200:
            return
        else:
            filename = self.link[self.link.rfind("/") + 1:]
            self.pricefile = tmp_folder + filename

            with open(self.pricefile, "wb") as f:
                f.write(response.content)

            logging.info('прайс %s скачан', self.id_provider)

            self.FileTypeRecognition()

        # читаем xlsx
        wb = xlrd.open_workbook(self.pricefile)
        sheet = wb.sheet_by_index(0)
        subgroup = ""

        # TODO: Надо выстроить условия в соответствии с прайсом.
        # во-первых qnt у нас теперь 9
        # во-вторых надо переделать файлы настройки
        # хорошо бы сделать универсальную функцию прогруза
        for row in range(8, sheet.nrows):
            if sheet.cell(row, int(self.qnt)).ctype == xlrd.XL_CELL_EMPTY:
                subgroup = str(sheet.cell(row, int(self.code)).value)
                # logging.info(subgroup)
            else:
                if sheet.cell(row, int(self.qnt)).ctype == xlrd.XL_CELL_NUMBER:
                    code = sheet.cell(row, int(self.code)).value
                    name = sheet.cell(row, int(self.name)).value
                    qnt = sheet.cell(row, int(self.qnt)).value
                    price = sheet.cell(row, int(self.price)).value
                    image = sheet.cell(row, int(self.image)).value
                    goodsgroup = subgroup
                    recommended_price = sheet.cell(row, int(self.recommended_price)).value
                    eancode = 0

                    # print(sheet.cell(row, int(self.code)).value, sheet.cell(row, int(self.name)).value,
                    #       sheet.cell(row, int(self.qnt)).value)
                    QuantsTable.UpdateQuants(code, name, self.stock_id, qnt, eancode)

            #         вот по сути просто грузить в БД осталось
            """
            if True:
                # print(sheet.cell(row, 0).value)
                if sheet.cell(row, int(self.qnt)).ctype == xlrd.XL_CELL_EMPTY:
                    subgroup = str(sheet.cell(row, int(self.code)).value)
                    print("subgroup", subgroup)
                else:
                    logging.info("здесь")
                    code = sheet.cell(row, int(self.code)).value
                    name = sheet.cell(row, int(self.name)).value
                    qnt = sheet.cell(row, int(self.qnt)).value
                    price = sheet.cell(row, int(self.price)).value
                    image = sheet.cell(row, int(self.image)).value
                    goodsgroup = subgroup
                    recommended_price = sheet.cell(row, int(self.recommended_price)).value
                    eancode = 0
                    # if sheet.cell(row, int(self.eancode)).ctype != xlrd.XL_CELL_EMPTY:
                    #     eancode = 0

                    print("code=", code, "name=", name, "id stock=", self.stock_id, "количество=", qnt, "qnt_type=", sheet.cell(row, int(self.qnt)).ctype, eancode)

                    # QuantsTable.UpdateQuants(code, name, self.stock_id, qnt, eancode)
            """

        logging.info('%s - затолкали', self.id_provider)



    def InsertSourseBattaries(self):
        """здесь заталкиваем Источник в БД"""

        response = requests.get(self.link, verify=False)

        if response.status_code != 200:
            logging.error('прайс %s недоступен', self.id_provider)
            return
        else:
            filename = self.link[self.link.rfind("/") + 1:]
            self.pricefile = tmp_folder + filename

            with open(self.pricefile, "wb") as f:
                f.write(response.content)

            logging.info('прайс %s скачан', self.id_provider)
            self.FileTypeRecognition()


        # читаем xlsx
        wb = xlrd.open_workbook(self.pricefile)
        sheet = wb.sheet_by_index(0)
        # logging.info("sb")


        for row in range (1, sheet.nrows):
            if (sheet.cell(row, 0).value != "category"):
                code = sheet.cell(row, int(self.code)).value
                name = sheet.cell(row, int(self.name)).value
                qnt = sheet.cell(row, int(self.qnt)).value
                price = sheet.cell(row, int(self.price)).value
                image = sheet.cell(row, int(self.image)).value
                goodsgroup = sheet.cell(row, int(self.goodsgroup)).value
                subgroup = sheet.cell(row, int(self.subgroup)).value
                recommended_price = 0
                packaging = int(sheet.cell(row, int(self.packaging)).value)
                eancode= 0
                if sheet.cell(row, int(self.eancode)).ctype != xlrd.XL_CELL_EMPTY:
                    eancode = str(sheet.cell(row, int(self.eancode)).value)

                QuantsTable.UpdateQuants(code, name, self.stock_id, qnt, eancode)
        logging.info('%s - затолкали', self.id_provider)


    def InsertCompel(self):
        """
        Закачиваем прайс компэла
        :return:


        """

        response = requests.get(self.link)
        rates = ExchangeRates()
        rateUSD = float(rates['USD'].value)

        if response.status_code != 200:
            logging.error("не качается", self.link)
            return

        d = response.headers['content-disposition']
        filename = re.findall("filename=(.+)", d)
        self.pricefile = tmp_folder + str(filename[0]).replace('"', '')

        with open(self.pricefile, "wb") as f:
            f.write(response.content)

        logging.info('прайс %s скачан', self.id_provider)

        self.FileTypeRecognition()

        pricelist = DBF(self.pricefile, load=True)
        for record in pricelist:
            code = str(record.get(self.code))
            name = str(record.get(self.name))
            qnt = int(record.get(self.qnt))
            price = float(record.get(self.price))*rateUSD
            goodsgroup = str(record.get(self.goodsgroup))
            subgroup = str(record.get(self.subgroup))
            # recommended_price = float(record.get(self.recommended_price))
            eancode = 0

            QuantsTable.UpdateQuants(code, name, self.stock_id, qnt, eancode)

        logging.info('%s - затолкали', self.id_provider)


    def InsertRct(self):
        """здесь заталкиваем радиотех в БД"""

        response = requests.get(self.link)
        rates = ExchangeRates()
        rateUSD = float(rates['USD'].value)

        if response.status_code != 200:
            return
        else:
            filename = self.link[self.link.rfind("/") + 1:]
            self.pricefile = tmp_folder + filename

            with open(self.pricefile, "wb") as f:
                f.write(response.content)

            logging.info('прайс %s скачан', self.id_provider)

            self.FileTypeRecognition()


        # читаем xlsx
        wb = xlrd.open_workbook(self.pricefile)
        sheet = wb.sheet_by_index(0)
        # logging.info("sb")


        for row in range (8, sheet.nrows):

            code = sheet.cell(row, int(self.code)).value
            name = sheet.cell(row, int(self.name)).value
            qnt = 0
            if sheet.cell(row, int(self.qnt)).ctype != xlrd.XL_CELL_EMPTY:
                qnt = int(sheet.cell(row, int(self.qnt)).value)

            price = sheet.cell(row, int(self.price)).value
            price = price*rateUSD

            image = 0
            goodsgroup = sheet.cell(row, int(self.goodsgroup)).value
            subgroup = sheet.cell(row, int(self.subgroup)).value
            recommended_price = 0
            # packaging = int(sheet.cell(row, int(self.packaging)).value)
            packaging = 0
            eancode= 0

            QuantsTable.UpdateQuants(code, name, self.stock_id, qnt, eancode)

        logging.info('%s - затолкали', self.id_provider)


    def __repr__(self):
        return "<Stock('%s', '%s')>" % (self.id_provider, self.link)

class DataExport(object):
    """
    класс для выгрузки потребностей
    """
    def __init__(self):

        if (config.read(config_file) == []):
            logging.error(' не могу открыть файл %s', config_file)

        try:
            files_config = config['files']

        except:
            logging.error('секция files отсутствует в файле %s', config_file)

        try:
            site_config = config['site']

        except:
            logging.error('секция site отсутствует в файле %s', config_file)

        self.needs_xlsx = files_config['needs']
        self.pricelist = files_config['pricelist']
        self.jsonpricelist = files_config['jsonpricelist']


        self.sitepricelist = site_config['sitepricelist']


        self.host = site_config['host']
        self.port = int(site_config['site_port'])
        self.site_passwd = site_config['site_passw']
        self.site_user = site_config['site_user']
        self.sitejsonpricelist = site_config['jsonpricelist']


    def XlsxAlignment(worksheet , addlength):
        """
        подпрограмка для выравнивания колонок в таблице, чтоб все буквы было видно
        worksheet  - таблица, которую надо изуродвать
        addlength - сколько добавить к колонке

        as_text - локальная функция для выяснения длины колонки

        :return: ту же таблицу, но выравненную
        """

        def as_text(value):
            if value is None:
                return ""
            return str(value)

        for column_cells in worksheet.columns:
            length = max(len(as_text(cell.value)) for cell in column_cells)
            worksheet.column_dimensions[column_cells[0].column].width = length+addlength


    def ExportNeeds(self):

        session = Session()

        #  создаем файл под потребности
        wb = Workbook()
        ws = wb.active

        ws.append(["группа", "код", "название", "в наличии", "мин", "макс", "поставщик", "имя у поставщика", "код постащика"])

        res = session.query(GoodGroupsTable.name, GoodsTable.code, GoodsTable.name, GoodsTable.qnt,
                            StockMinMaxTable.minqnt, StockMinMaxTable.maxqnt, FacesTable.name, Good_AltnamesTable.name, Good_AltnamesTable.code)\
            .join(GoodsTable, GoodsTable.group_id == GoodGroupsTable.id)\
            .join(StockMinMaxTable, StockMinMaxTable.good_id == GoodsTable.id)\
            .join(Good_AltnamesTable, Good_AltnamesTable.good_id == GoodsTable.id)\
            .join(FacesTable, FacesTable.id == Good_AltnamesTable.face_id)\
            .filter(StockMinMaxTable.minqnt.isnot(None)).filter(StockMinMaxTable.minqnt > 0)\
            .filter(StockMinMaxTable.maxqnt.isnot(None)).filter(StockMinMaxTable.maxqnt > 0)\
            .filter(GoodsTable.qnt <= StockMinMaxTable.minqnt)

        for row in res:
            ws.append(list(row))

        # выравниваем таблицу
        DataExport.XlsxAlignment(ws, 1)
        # Записываем xlsx

        wb.save(self.needs_xlsx)
        session.close_all()
        logging.info('файл потребностей сохранен: %s', self.needs_xlsx)


    def ExportPrice(self):
        """
        Выгрузка прайса в эксель на сайт
        Выгрузку прайса в json на einfo
        :return:
        """
        """
        прайсы во Фрегате храняться в таблице prices
        """

        # Файл прайс-листа
        wb = Workbook()
        ws = wb.active

        session = Session()
        transport = paramiko.Transport(self.host, self.port)
        transport.connect(username=self.site_user, password=self.site_passwd)
        sftp = paramiko.SFTPClient.from_transport(transport)

        # Заголовок прайса
        ws.append(["группа","название", "код", "цена розн", "цена опт", "количество", "срок поставки"])


        # открываем (создаем)json
        js = list()
        jsonstr = dict()

        fjs = open(self.jsonpricelist, "w")

        for goodname, goodcode, good_id, goodsgroup, qnt, stock_id \
                in session.query(GoodsTable.name, GoodsTable.code, GoodsTable.id,
                                 GoodGroupsTable.name, QuantsTable.qnt, QuantsTable.stock_id)\
                .join(GoodGroupsTable, GoodGroupsTable.id == GoodsTable.group_id)\
                .join(QuantsTable, QuantsTable.good_id == GoodsTable.id)\
                .filter(QuantsTable.qnt > 0) \
                .filter(GoodGroupsTable.id != 80004) \
                .filter(GoodGroupsTable.id != 29770) \
                .filter(GoodGroupsTable.id != 1932) \
                .filter(GoodGroupsTable.id != 2132) \
                .filter(GoodGroupsTable.id != 2037) \
                .filter(GoodGroupsTable.id != 2129) \
                .filter(GoodGroupsTable.id != 2042) \
                .filter(GoodGroupsTable.id != 2103) \
                .order_by(GoodGroupsTable.name):

            retailprice = 0
            wholesaleprice = 0
            dtmax = datetime.date.fromordinal(1)
            deliverytime = '3-7 раб.дней'
            delivery_json = 1
            # 37 - наш склад
            if stock_id == 37:
                deliverytime = 'В наличии'
                delivery_json = 0


            for res in session.query(PricesTable.good_id, PricesTable.dt, PricesTable.pricelist_id,
                                     PricesTable.price_i).filter(PricesTable.good_id == good_id):
                if dtmax < res.dt:
                    dtmax = res.dt
                    if res.pricelist_id == 1:
                        retailprice = res.price_i
                    elif res.pricelist_id == 2:
                        wholesaleprice = res.price_i

                    if retailprice < wholesaleprice:
                        retailprice = wholesaleprice

            qnt = int(qnt)
            record_string = (goodsgroup, goodname, goodcode, retailprice, wholesaleprice, qnt, deliverytime)

            ws.append(record_string)


            jsonstr = dict([("title", goodname),
                            ("category", goodsgroup),
                            ("price_opt", wholesaleprice),
                            ("price_roz", retailprice),
                            ("stock", int(qnt)),
                            ("delivery", delivery_json)])

            print(json.dumps(jsonstr, ensure_ascii=False), file=fjs)

        #  ширина колонок
        DataExport.XlsxAlignment(ws, 1)


        wb.save(self.pricelist)
        logging.info(' %s - сделано', self.pricelist)

        sftp.put(self.pricelist, self.sitepricelist)
        sftp.put(self.jsonpricelist, self.sitejsonpricelist)
        sftp.close()

        logging.info('прайс-листы успешно загружены по адресу %s, %s', self.sitepricelist, self.sitejsonpricelist)




if __name__ == "__main__":

    # BarcodeTable.SaveBarcode(638835, '123489')




    # # Загрузка внешних складов
    # # убираем старые данные
    QuantsTable.DeleteOldGoods()

    prov = Provider('sourcebatteries')
    prov.SavePrice()

    prov = Provider('zip-2002')
    prov.SavePrice()

    prov = Provider('sds')
    prov.SavePrice()


#    prov = Provider('compel')
#    prov.SavePrice()


#    prov = Provider('rct')
#    prov.SavePrice()


    # выгрузка потребности в эксельку
    # n = DataExport()
    # n.ExportNeeds()

    # n.ExportPrice()

